// create the module and name it phpro
// also include ngRoute for all our routing needs
var homepage = angular.module('homepage', ['ngRoute']);
var Ecom_Api = "http://localhost:56124/Ecom/";
var Users_path = "F:/Angularproject/Users.json";
var Orders_path = "F:/Angularproject/Orders.json";
var validemail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
var validmobile= /^[1-9]{1}[0-9]{9}$/;
homepage.run(function ($rootScope) {
    $rootScope.cat_baseurl = '/Category_Images/';
    $rootScope.item_baseurl = '/Item_Images/';
    $rootScope.Catlist = {};
    $rootScope.Itemlist = {};
    $rootScope.linkeddata = {};
    $rootScope.loginuser = {};
    $rootScope.Cart = {};
    if (localStorage.cart == undefined || localStorage.loginuser == undefined) {
        localStorage.cart = EncryptData(JSON.stringify({}));
        localStorage.loginuser = EncryptData(JSON.stringify({}));
    }
    if (jQuery.isEmptyObject(JSON.parse(DycryptData(localStorage.cart)))) {
        localStorage.cart = EncryptData(JSON.stringify({}));
    }
    
    $rootScope.show =true;
});
// configure our routes
homepage.config(function ($routeProvider) {

    $routeProvider
         .when('/', {
             templateUrl: 'templates/categories.html',
             controller: 'catCtrl'
         })
            // route for the FAQ page
            .when('/cat', {

                templateUrl: 'templates/categories.html',
                controller: 'catCtrl'
            })

            // route for the contact page
            .when('/subcat', {
                templateUrl: 'templates/subcategories.html',
                controller: 'subcatCtrl'
            })
        .when('/items', {
            templateUrl: 'templates/items.html',
            controller: 'itemCtrl'
        })
        .when('/details', {
            templateUrl: 'templates/details.html',
            controller: 'detailCtrl'
        })
        .when('/cart', {
            templateUrl: 'templates/cart.html',
            controller: 'cartCtrl'
        })
    .when('/checkout', {
        templateUrl: 'templates/checkout.html',
        controller: 'checkoutCtrl'
    })
     .when('/sucess', {
         templateUrl: 'templates/sucess.html',
         controller: 'sucessCtrl'
     })
        .when('/login', {
            templateUrl: 'templates/login.html',
            controller: null
        });
});

// create the controller and inject Angular's $scope
homepage.controller('mainCtrl', function ($scope) {
        // create a message to display in our view
        $scope.heading = 'Welcome to PHPRO.ORG';
        $scope.message = 'Here you will find the meaning of life.';
});

homepage.controller('catCtrl', function ($scope) {

    
    $scope.Getcatdata();
});

homepage.controller('subcatCtrl', function ($scope, $rootScope) {
    var Info = localStorage.SelectionInfo;
    var filterlist = [];
    var SouceId = Info.split("-")[0];
    var ParentId = Info.split("-")[1];
    var Level = parseInt(Info.split("-")[2]) + 1;
   
    for (var y = 0; y < $rootScope.linkeddata.length; y++) {

        if ($rootScope.linkeddata[y].SouceId == SouceId && $rootScope.linkeddata[y].ParentId == ParentId && $rootScope.linkeddata[y].Level == Level) {

            filterlist.push($rootScope.linkeddata[y].ChildId);
          
        }

    }


    var datajson = $rootScope.Catlist;

    var totalchilds = [];
    for (var z = 0; z < datajson.length; z++) {

        for (k = 0; k < filterlist.length; k++) {
            if (datajson[z].Id == filterlist[k]) {
                var id = SouceId + '-' + datajson[z].Id + '-' + Level;
                item = {};
                item["Id"] = id;
                for (var key in datajson[z]) {
                    if (key != "Id")
                        item[key] = datajson[z][key]

                }

                totalchilds.push(item);

            }
        }

    }
    console.log('child-list', totalchilds)
    $rootScope.show = 3;
    $scope.subcatdata = totalchilds;

});

homepage.controller('itemCtrl', function ($scope, $rootScope) {
    var Info = localStorage.SelectionInfo;
    var filterlist = [];
    var SouceId = Info.split("-")[0];
    var ParentId = Info.split("-")[1];
    var Level = parseInt(Info.split("-")[2]) + 1;
    for (var y = 0; y < $rootScope.linkeddata.length; y++) {

        if ($rootScope.linkeddata[y].SouceId == SouceId && $rootScope.linkeddata[y].ParentId == ParentId && $rootScope.linkeddata[y].Level == Level) {

            filterlist.push($rootScope.linkeddata[y].ChildId);
           
        }

    }


    var datajson =  $rootScope.Itemlist;

    var totalchilds = [];
    for (var z = 0; z < datajson.length; z++) {

        for (k = 0; k < filterlist.length; k++) {
            if (datajson[z].Id == filterlist[k]) {
                var id = SouceId + '-' + datajson[z].Id + '-' + Level;
                item = {};
                item["Id"] = id;
                for (var key in datajson[z]) {
                    if (key != "Id")
                        item[key] = datajson[z][key]

                }

                totalchilds.push(item);

            }
        }

    }
    console.log('child-list', totalchilds)
        $rootScope.show = 3;
        $scope.itemdata = totalchilds;
   
});



homepage.controller('detailCtrl', function ($scope, $rootScope) {
       
    var proid = localStorage.ProductId;
    for (var y = 0; y < $rootScope.Itemlist.length; y++) {
        if ($rootScope.Itemlist[y].Id == proid) {
            $rootScope.itemdetail = [$rootScope.Itemlist[y]];
            return;
        }
    }
   
});

homepage.controller('cartCtrl', function ($scope, $rootScope) {

    if (jQuery.isEmptyObject(JSON.parse(DycryptData(localStorage.cart))))
    {

    }
    else {
        cartbild = [];
        for (var c = 0; c < JSON.parse(DycryptData(localStorage.cart)).length; c++) {

            for(var d=0;d<$rootScope.Itemlist.length;d++){
                if (JSON.parse(DycryptData(localStorage.cart))[c].Id == $rootScope.Itemlist[d].Id)
                {
                    $rootScope.Itemlist[d].Qty = JSON.parse(DycryptData(localStorage.cart))[c].Qty;
                    
                    cartbild.push($rootScope.Itemlist[d]);
                }
            }
            

        }
        console.log("cartlist",cartbild)
        $scope.cartdetail = cartbild;
    }

});

homepage.controller('checkoutCtrl', function ($scope, $rootScope) {
    if (jQuery.isEmptyObject(JSON.parse(DycryptData(localStorage.loginuser)))) {
        window.location = "#/login";
    }
    else
    {
        $scope.addressdata = [JSON.parse(DycryptData(localStorage.loginuser))];
        window.location = "#/checkout";

    }

});
homepage.controller('sucessCtrl', function ($scope, $rootScope) {


})


homepage.controller('HomeCtrl', function ($scope, $http, $rootScope, $filter, $compile) {
    $scope.$watch(function () {
       
        if (jQuery.isEmptyObject(JSON.parse(DycryptData(localStorage.loginuser))))
        {
            $rootScope.show = true;
        }
        else {
            $rootScope.show = false;
            $scope.loginuserid = JSON.parse(DycryptData(localStorage.loginuser))["email"];
        }
    });
   
    $http.get("/Category.json")
    .then(function (response) {
        $rootScope.Catlist = response.data.records;
    });
    $http.get("/Items.json")
   .then(function (response) {
       $rootScope.Itemlist = response.data.records;
   });
    $http.get("/Linking.json")
    .then(function (response) {

        $rootScope.linkeddata = response.data.records;
    });
    $scope.Getcatdata = function () {
        $scope.catdata = $rootScope.Catlist;
   
    }
    $scope.logout = function ($event) {
        $rootScope.show = true;
        $rootScope.loginuser = {};

        localStorage.loginuser = {};
    }
    $scope.User_login = function ($event) {
        var email = $('#l_email').val();
        var password = $('#l_password').val();
        var status = false;
        if (email.trim() == "" && password.trim() == "") {
            $.growl.error({ message: "Invalid Credentials!" });
            return;
        }
        if (!validemail.test(email)) {
            $.growl.error({ message: "Enter Valid Email id!" });
            return;
        }
        
       // $.growl.notice({ message: "The kitten is cute!" });
       // $.growl.warning({ message: "The kitten is ugly!" });
        $http.get("/Users.json")
    .then(function (response) {
      
        for (var i = 0; i < response.data.records.length; i++) {
            if (response.data.records[i].email == email && response.data.records[i].password == password) {
                status = true;
                $rootScope.loginuser["id"] = response.data.records[i].id;
                $rootScope.loginuser["name"] = response.data.records[i].name;
                $rootScope.loginuser["email"] = response.data.records[i].email;
                $rootScope.loginuser["mobile"] = response.data.records[i].mobile;
                $rootScope.loginuser["address"] = response.data.records[i].address;
                $rootScope.loginuser["city"] = response.data.records[i].city;
                $rootScope.loginuser["state"] = response.data.records[i].state;
                $rootScope.loginuser["pincode"] = response.data.records[i].pincode;
                $rootScope.show = false;
                localStorage.loginuser = EncryptData(JSON.stringify($rootScope.loginuser));
                window.history.back();
                return;

            }
        }
        if (status == false) {
            $.growl.error({ message: "Invalid Credentials!" });
            return;
        }

    });
    }
    $scope.User_register = function ($event) {


        if ($("#r_username").val().trim() == "") {
            $.growl.error({ message: "Enter Username !" });
            return;
        }
        else if (($("#r_email").val().trim() == "") && (!validemail.test($("#r_email").val())) ) {
            $.growl.error({ message: "Enter Valid Emailid !" });
            return;
        }
        else if ($("#r_mobile").val().trim() == "" && !validmobile.test($("#r_email").val())) {
            $.growl.error({ message: "Enter Valid Mobile Number !" });
            return;
        }

        else if ($("#r_password").val().trim() == "") {
            $.growl.error({ message: "Enter Password !" });
            return;
        }
        else if ($("#r_confirm-password").val().trim() == "") {
            $.growl.error({ message: "Enter Confirm Password !" });
            return;
        }
        else if ($("#r_password").val() != $("#r_confirm-password").val()) {
            $.growl.error({ message: "Password and Confirm Password should be same !" });
            return;
        }
        else if ($("#r_address").val().trim() == "") {
            $.growl.error({ message: "Enter Address !" });
            return;
        }
        else if ($("#r_city").val().trim() == "") {
            $.growl.error({ message: "Enter City !" });
            return;
        }
        else if ($("#r_state").val().trim() == "") {
            $.growl.error({ message: "Enter State !" });
            return;
        }
        else if ($("#r_pincode").val().trim() == "" && parseInt($("#r_pincode").val().length)!=6 ) {
            $.growl.error({ message: "Enter Valid Pincode !" });
            return;
        }


        users = {}
        users["name"] = $("#r_username").val();
        users["email"] = $("#r_email").val();
        users["mobile"] = $("#r_mobile").val();
        users["password"] = $("#r_password").val();
        users["address"] = $("#r_address").val();
        users["city"] = $("#r_city").val();
        users["state"] = $("#r_state").val();
        users["pincode"] = $("#r_pincode").val();
        

        var userdata;
        $http.get("/Users.json")
   .then(function (response) {
      
       if(response.data.records!=undefined)
       {
           userdata = response.data.records;
           userdata.push(users);
       }
       else {
           userdata={ };
           userdata = [users];
       }

       item = {}
       item["records"] = userdata;
       $scope.Writedata(Users_path, item);
   });

       

    }
    
    $scope.Writedata = function (path,data) {

        $.ajax({
            url: Ecom_Api + 'Write?path=' + path + '&json=' + JSON.stringify(data),
            type: 'POST',
            success: function (response) {

               
            },
            error: function () {
            }
        });

    }
    $scope.displaychild = function ($event) {
       
        var Info = $event.currentTarget.id;
        localStorage.SelectionInfo = Info;

        var SouceId = Info.split("-")[0];
        var ParentId = Info.split("-")[1];
        var Level = parseInt(Info.split("-")[2]) + 1;
        var lookinto = 0;
        for (var y = 0; y < $rootScope.linkeddata.length; y++) {

            if ($rootScope.linkeddata[y].SouceId == SouceId && $rootScope.linkeddata[y].ParentId == ParentId && $rootScope.linkeddata[y].Level == Level) {

                lookinto = $rootScope.linkeddata[y].Group;
                break;
            }

        }

        if (lookinto == 1) {
            window.location = "#/subcat";

        } else {
            window.location = "#/items";
        }
    }

    $scope.Itemdetails = function ($event) {
        window.location = "#/details";
        var Info = $event.currentTarget.id;
        localStorage.ProductId = Info.split("-")[1]

    }

    $scope.Addtocart = function ($event) {
        newItem = {};
        newItem["Id"] = $event.currentTarget.name;
        newItem["Qty"] = 1;
        
        if (jQuery.isEmptyObject(JSON.parse(DycryptData(localStorage.cart))))
        {
            localStorage.cart =EncryptData(JSON.stringify([newItem]));
        }
        else
        {
            var addtocart = 0;
            for (var x = 0; x < JSON.parse(DycryptData(localStorage.cart)).length; x++) {
                if (JSON.parse(DycryptData(localStorage.cart))[x].Id == $event.currentTarget.name) {
                    addtocart = 1;
                }
            }
            if (addtocart == 0) {
                var cart = JSON.parse(DycryptData(localStorage.cart));
               
                cart.push(newItem);
                localStorage.cart = EncryptData(JSON.stringify(cart));
            }
        }
    
        window.location = "#/cart";
    }




    $scope.Removetocart = function ($event) {

        for (var r = 0; r < $rootScope.Cart.length; r++) {

            if ($rootScope.Cart[r].Id == $event.currentTarget.name) {

                $rootScope.Cart.splice(r, 1);
                

               // return;
            }
        }
       
    }

    $scope.Nextstep = function ($event) {

        $('.activetab').removeClass('activetab');
        var activepanel = '#panel' + $event.currentTarget.name + ' ' + '.panel-heading';
        $(activepanel).addClass('activetab');
        if ($event.currentTarget.name > 1) {
            for (var c = $event.currentTarget.name; c > 1; c--) {
               
                var hideval = '#panel' + (c - 1) + ' ' + '.change';
                $(hideval).css('display', 'block');
            }
            
        }

        for (var j = 1; j < 4; j++)
        {
            if (j == $event.currentTarget.name) {
                $("#collapse" + $event.currentTarget.name).addClass('in');
                if (j == 3) {
                    $scope.totalamount = $scope.getTotal();
                }
            }
            else
            {
                $("#collapse" + j).removeClass('in');
            }
        }
       
    }

    $scope.getTotal = function () {
        var totalprice = 0;
        for (var c = 0; c < JSON.parse(DycryptData(localStorage.cart)).length; c++) {

            for (var d = 0; d < $rootScope.Itemlist.length; d++) {
                if (JSON.parse(DycryptData(localStorage.cart))[c].Id == $rootScope.Itemlist[d].Id) {
                    totalprice += (parseFloat($rootScope.Itemlist[d].Price) * parseInt(JSON.parse(DycryptData(localStorage.cart))[c].Qty));


                }
            }


        }
        return totalprice;
    }
    $scope.PlaceOrder = function ($event) {

        var currentdate = new Date();
        var datetime = currentdate.getDate() + "/"
                    + (currentdate.getMonth() + 1) + "/"
                    + currentdate.getFullYear() + " @ "
                    + currentdate.getHours() + ":"
                    + currentdate.getMinutes() + ":"
                    + currentdate.getSeconds();
        neworder = {}
        neworder["UserId"] = JSON.parse(DycryptData(localStorage.loginuser))["id"];
        var OrderNumber=Math.floor(Math.random() * 1E6);
        neworder["OrderNumber"] = OrderNumber;
        neworder["OrderDetail"] = $rootScope.Cart;
        neworder["Date"] = datetime;
        neworder["StageId"] = 1;
        neworder["StateId"] = 1;


        $http.get("/Orders.json")
    .then(function (response) {
        var orders = response.data.records;

        if (orders != undefined) {
            neworder["OrderId"] = (parseInt(response.data.records.length) + 1);
            orders.push(neworder);

        } else
        {
            neworder["OrderId"] = 1;
            orders = {};
            orders = [neworder];
        }
        item = {}
        item["records"] = orders;
        console.log("orderlist",item)
        $scope.Writedata(Orders_path, item);
        window.location = "#/sucess";
        $scope.OrderNumber = OrderNumber;
    });
    }
});



