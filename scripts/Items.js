var app = angular.module('EcomApp', []);
app.run(function ($rootScope) {
    $rootScope.show = 1;
    $rootScope.Catlist = {};
    $rootScope.Itemlist = {};
    $rootScope.linkeddata = {};
})



app.controller('CategoryCtrl', function ($scope, $http, $rootScope, $filter) {
   
    $rootScope.show = 1;
    $http.get("/Category.json")
    .then(function (response) {
        $scope.catdata = response.data.records;
        $rootScope.Catlist = response.data.records;
    });
    $http.get("/Items.json")
   .then(function (response) {
       $rootScope.Itemlist = response.data.records;
   });
    $http.get("/Linking.json")
    .then(function (response) {

        $rootScope.linkeddata = response.data.records;
    });
    $scope.displaychild = function ($event) {
        var Info = $event.currentTarget.id;
       
        var filterlist = [];
       
        var SouceId = Info.split("-")[0];
        var ParentId = Info.split("-")[1];
        var Level =parseInt(Info.split("-")[2]) + 1;
        var lookinto = 0;

        for (var y = 0; y < $rootScope.linkeddata.length; y++) {

            if ($rootScope.linkeddata[y].SouceId == SouceId && $rootScope.linkeddata[y].ParentId == ParentId && $rootScope.linkeddata[y].Level == Level) {

                filterlist.push($rootScope.linkeddata[y].ChildId);
                lookinto = $rootScope.linkeddata[y].Group;
            }

        }


         var datajson = "";
         if (lookinto == 1) {

             datajson = $rootScope.Catlist;

      }else
      {
             datajson = $rootScope.Itemlist;
      }

         var totalchilds = [];
         for (var z = 0; z < datajson.length; z++) {

             for (k = 0; k < filterlist.length; k++) {
                 if (datajson[z].Id == filterlist[k]) {
                     var id = SouceId + '-' + datajson[z].Id + '-' + Level;
                     item = {};
                     item["Id"] = id;
                     for (var key in datajson[z]) {
                         if (key != "Id")
                             item[key] = datajson[z][key]
                        
                     }
                     
                     totalchilds.push(item);

                 }
             }

         }
         console.log('child-list', totalchilds)
         if (lookinto == 1) {
             $rootScope.show = 2;
             $scope.subcatdata = totalchilds;
         }
         else {
             $rootScope.show = 3;
             $scope.itemdata = totalchilds;
         }
    }

    $scope.login=function()
    {
       
        $rootScope.show = 3;


    }

    
   

    
});



