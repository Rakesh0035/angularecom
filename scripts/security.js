﻿function EncryptData(str) {
    var sb = '';
    for (var i = 0; i < str.length; i++) {
        var v = parseInt(str.charCodeAt(i));
        v = v.toString(16);
        while (v.length < 2) {
            v = '0' + v;
        }
        sb += v.toUpperCase();

    }
    return sb;
}

function DycryptData(hexStr) {
    var sb = '';
    for (var i = 0; i < hexStr.length; i += 2) {
        var n = parseInt(hexStr.substr(i, 2), 16);
        sb += String.fromCharCode(n);
    }
    return sb;
}