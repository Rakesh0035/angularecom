var app = angular.module('EcomApp', []);

//constansts
var Ecom_Api = "http://localhost:56124/Ecom/"; 
var Items_path = "F:/Angularproject/Item_Images/";
var Category_path = "F:/Angularproject/Category_Images/";
var Items_list = "F:/Angularproject/Items.json";
var Category_list = "F:/Angularproject/Category.json";
var Link_list = "F:/Angularproject/Linking.json";
var Caterogy_path="F:/Angularproject/Category_Images/";
//constansts
app.run(function ($rootScope) {
    $rootScope.Admincategorylist = {};
    $rootScope.AdminItemslist = {};
    $rootScope.attrgroups = {};
    $rootScope.show = 1;
    $rootScope.catlist = {};
    $rootScope.itemlist = {};
    $rootScope.attrlist = {};
    $rootScope.linkinglist = {};

});

app.controller('AdminCtrl', function ($scope, $http, $rootScope, $filter, $compile) {
   
    $rootScope.show = 1;
    $http.get("/AdminCategory.json")
    .then(function (response) {
       
        console.log(response)
        $scope.catdata = response.data.records;
        $rootScope.Admincategorylist = response.data.records;
    });
    $http.get("/AdminItems.json")
    .then(function (response) {

        console.log(response)
        $scope.itemdata = response.data.records;
        $rootScope.AdminItemslist = response.data.records;
       
    });
    $http.get("/Category.json")
   .then(function (response) {

       $rootScope.catlist = response.data.records;

   });

    $http.get("/Items.json")
  .then(function (response) {
      if (response.data!="")
      $rootScope.itemlist = response.data.records;

  });

    $http.get("/Attribute.json")
 .then(function (response) {

     $rootScope.attrlist = response.data.records;
    
 });
    $http.get("/AttributeGroups.json")
.then(function (response) {

    $rootScope.attrgroups = response.data.records;
    $scope.attrdata = response.data.records;
});
    $http.get("/Linking.json")
 .then(function (response) {

     $rootScope.linkinglist = response.data.records;

 });

    $scope.links = function (showid) {
        $rootScope.show = showid;
        if(showid==3)
        {
            $scope.addattr();
        }
    }

    $scope.addattr = function () {
        var attributes = $('#attrgroup ' + 'option:selected').val();
        attributes = JSON.parse(attributes);
      
        $('#dynamicattr').html("");
      
        for (var a = 0; a < attributes.length; a++) {
            for (var b = 0; b < $rootScope.attrlist.length; b++) {
                if ($rootScope.attrlist[b].Id == attributes[a]) {
                    var newattr = '<div class="form-group"><label name=' + $rootScope.attrlist[b].Name + ' class="control-label col-sm-2" >' + $rootScope.attrlist[b].Name + ':</label><div class="col-sm-offset-2 col-sm-10">';
                    newattr += '<input  type="' + $rootScope.attrlist[b].Type + '" name="Item-' + $rootScope.attrlist[b].Name + '"  class="form-control"></div></div>'
                    $('#dynamicattr').append(newattr);
                }
            }


        }
     
        
    }

    $scope.createattrgroup = function () {
        item = {}
        var attr = [];
        $("#attrlist option").each(function () {

            attr.push($(this).val())
            // Add $(this).val() to your list
        });
        item = {}

        item["Id"] = 0;
        item["Name"] =$('#groupname').val();
        item["List"] = attr;

       
        $rootScope.attrgroups.push(item);
        console.log("new", $rootScope.attrgroups)
    }

    $scope.createcategory=function()
    {
        $('#CategoryForm input[type="file"]').each(function () {
            var data = new FormData();

            data.append("Path", Category_path);
            var files = $(this).get(0).files;
            // var files = $('#ItemForm input[type="file"]').get(0).files;

            if (files.length > 0) {
                data.append("UploadedImage", files[0]);
            }
            $.ajax({
                url: Ecom_Api + 'Image',
                type: 'POST',
                contentType: false,
                processData: false,
                data: data,
                success: function (response) {

                    
                },
                error: function () {
                }
            });
            // Make Ajax request with the contentType = false, and procesDate = false

        })

        item = {}
        
        if ($rootScope.catlist != undefined) {
            item["Id"] = parseInt($rootScope.catlist.length) + 1;
        }
        else {
            item["Id"] = 1;
            
        }
        

       
        var AdminCatlist = $rootScope.Admincategorylist;
       
        for (var i = 0; i < AdminCatlist.length; ++i) {

            if ($('#cat' + AdminCatlist[i].Id).attr('type') == "file") {
                item[AdminCatlist[i].Name] = $('#cat' + AdminCatlist[i].Id)[0].files[0].name;
            }
            else
            {
                item[AdminCatlist[i].Name] = $('#cat' + AdminCatlist[i].Id).val();
            }

        }
        if ($rootScope.catlist != undefined) {
            $rootScope.catlist.push(item);
        }
        else {
            $rootScope.catlist = {};
            $rootScope.catlist = [item];
        }
        item = {}
        item["records"] = $rootScope.catlist;
        var jsondata = encodeURIComponent(JSON.stringify(item));
        $.ajax({
            url: Ecom_Api + 'Write',
            type: 'POST',
            data: "path=" + Category_list+"&json=" + jsondata,
            success: function (response) {

                
            },
            error: function () {
            }
        });
      
    }


    $scope.createitem = function () {

       
        $('#ItemForm input[type="file"]').each(function () {
            var data = new FormData();

            data.append("Path", Items_path);
            var files =  $(this).get(0).files;
       // var files = $('#ItemForm input[type="file"]').get(0).files;

        if (files.length > 0) {
            data.append("UploadedImage", files[0]);
        }
        $.ajax({
            url: Ecom_Api+'Image',
            type: 'POST',
            contentType: false,
            processData: false,
            data: data,
            success: function (response) {

               
            },
            error: function () {
            }
        });
        // Make Ajax request with the contentType = false, and procesDate = false
       
        })

        item = {}
        if ($rootScope.itemlist.length != undefined) {

            
            item["Id"] = parseInt($rootScope.itemlist.length) + 1;
        }
        else {
            item["Id"] = 1;
        }
       
        $('#ItemForm label').each(function () {
            if ($(this).attr('name') != "Attribute")
            {
              
                if ($("input[name=Item-" + $(this).attr('name') + "]").attr('type') == "file") {
                    item[$(this).attr('name')] = $("input[name=Item-" + $(this).attr('name') + "]")[0].files[0].name;
                }
                else {
                    item[$(this).attr('name')] = $("input[name=Item-" + $(this).attr('name') + "]").val();
                }
            }
            else {
                item[$(this).attr('name')] = $('#attrgroup').find(":selected").data('id');
            }
            
           
            
        })
        if ($rootScope.itemlist.length != undefined) {
            $rootScope.itemlist.push(item);
           
        }
        else
        {
            $rootScope.itemlist = {};
            $rootScope.itemlist = [item];
        }
        item = {}
        item["records"] = $rootScope.itemlist;
        console.log("final", item)
        var jsondata = encodeURIComponent(JSON.stringify(item));
        $.ajax({
            url: Ecom_Api + 'Write',
            type: 'POST',
            data: "path=" + Items_list + "&json=" + jsondata,
            success: function (response) {

                alert(response)
            },
            error: function () {
            }
        });





    }

    $scope.groups = [{ "Id": "1", "Name": "Category" },{ "Id": "2", "Name": "Products" }];
    $scope.linking = function () {
        $rootScope.show = 2;

        
        $scope.Maincatdata = $rootScope.catlist;
        $scope.Getdata = function ($event) {
          
            var filterlist = [];
            var lookinto = "1";
            var attrid = $event.target.name;
            var info = $event.target.id;
            var SouceId = info.split("-")[0];
            var ParentId = info.split("-")[1];
            
            var Level = info.split("-")[2];
            if (Level == 1) {
                 $('.infodiv').html('');
            }
            if ($rootScope.linkinglist != undefined) {
                var linkeddata = $rootScope.linkinglist;
                for (var y = 0; y < linkeddata.length; y++) {

                    if (linkeddata[y].SouceId == SouceId && linkeddata[y].ParentId == ParentId && linkeddata[y].Level == Level) {
                        filterlist.push(linkeddata[y].ChildId);
                        lookinto = linkeddata[y].Group;
                    }

                }
                console.log("push", filterlist)

            }
            
            var html = "<select id=" + 'groupid' + info + " data-attrid=" + attrid + " name=" + info + " ng-click='Filldropdown($event);'><option value='1'>category</option><option value='2'>product</option></select>";
            html += "<select id=" + 'group' + info + ">";
            html += Groupdata("1", ParentId, filterlist, attrid);
             html += "</select>";
             html += "<input type='button' name=" + info + " value='Add' ng-click='createlink($event);'/>"
             html += "<ul id=" + 'ul' + info + "></ul>";
             $('#info' + info).html("");
             var temp2 = $compile(html)($scope);
             $('#info' + info).append(temp2);
            
           


      var datajson = "";
      if (lookinto == 1) {

          datajson = $rootScope.catlist;

      }else
      {
          datajson = $rootScope.itemlist;
      }
    
     for (var z = 0; z < datajson.length; z++) {

         for (k = 0; k < filterlist.length; k++) {
             if (datajson[z].Id == filterlist[k]) {
                 
                 
                 var id = SouceId + '-' + datajson[z].Id + '-' + (parseInt(Level) + 1);
                 var deleteid = SouceId + '-' + ParentId + '-' + datajson[z].Id;
                 var sub = "<li id="+deleteid+">";
                 if (lookinto == 1) {
                     sub += "<a href='#' id=" + id + " name=" + datajson[z].Attribute + " ng-click='Getdata($event);'>" + datajson[z].Name + "</a><img name=" + deleteid + "  class='cancel' ng-click='deletelink($event);' src='/icons/cancel.png'/>";
                 }
                 else
                 {
                     sub += "<a href='#' id=" + id + " >" + datajson[z].Name + "</a><img name=" + deleteid + "  class='cancel' ng-click='deletelink($event);' src='/icons/cancel.png'/>";
                 }
                 id = "info" + id;
                  sub += "<div id="+id+"></div>";
                 
                  sub += "</li>";
                  var temp = $compile(sub)($scope);
                 $('#ul' + info).append(temp)

             }
         }

     }
    

        }
        $scope.Filldropdown = function ($event) {

            var filterlist = [];
            var lookinto = $('#' + $event.target.id).find('option:selected').val();
            var info = $event.target.name;
            var SouceId = info.split("-")[0];
            var ParentId = info.split("-")[1];
            var Level = info.split("-")[2];
            var attrid = $('#' + $event.target.id).data('attrid');
           
            if ($rootScope.linkinglist != undefined) {
                var linkeddata = $rootScope.linkinglist;
                for (var y = 0; y < linkeddata.length; y++) {

                    if (linkeddata[y].SouceId == SouceId && linkeddata[y].ParentId == ParentId && linkeddata[y].Level == Level) {
                        filterlist.push(linkeddata[y].ChildId);

                    }

                }
                console.log("push", filterlist)
            }
            
            //alert($event.target.name)

            $('#group' + $event.target.name).html(Groupdata(lookinto, ParentId, filterlist, attrid));



        }
        $scope.createlink = function ($event) {
           
            var value = $event.target.name;
            var newlink = {
                "Id": "0",
                "SouceId": "0",
                "ParentId": "0",
                "ChildId": "0",
                "Level": "0",
                "Group": "0"

            };
            var NewId = 0;
            if ($rootScope.linkinglist != undefined) {

                 NewId = parseInt($rootScope.linkinglist.length) + 1;
            }
            else {
                NewId = 1;
            }
            newlink.Id = NewId;
            newlink.SouceId = value.split("-")[0];
            newlink.ParentId = value.split("-")[1];
            newlink.ChildId = $('#group' + value).val();
            newlink.Level = (parseInt(value.split("-")[2]));
            newlink.Group = $('#groupid' + value).val();

            var deleteid = value.split("-")[0] + "-" + value.split("-")[1] +"-"+ $('#group' + value).val();
            console.log(newlink)
            if ($rootScope.linkinglist != undefined) {
                $rootScope.linkinglist.push(newlink);
            }
            else {
                $rootScope.linkinglist = {};
                $rootScope.linkinglist = [newlink];
            }
            item = {}
            item["records"] = $rootScope.linkinglist;
            $.ajax({
                url: Ecom_Api + 'Write?path=' + Link_list + '&json=' + JSON.stringify(item),
                type: 'POST',
                success: function (response) {

                    alert(response)
                },
                error: function () {
                }
            });


            var latestid = value.split("-")[0] + "-" + $('#group' + value).val() + "-" + (parseInt(value.split("-")[2]) + 1);
            var newdata = "";
            if ($('#groupid' + value).val() == "1") {
                newdata = "<li id=" + deleteid + "><a href='#' id=" + latestid + " ng-click='Getdata($event);'>" + $('#group' + value).find('option:selected').text() + "</a><img name=" + deleteid + "  class='cancel' ng-click='deletelink($event);' src='/icons/cancel.png'/><div id=" + 'info' + latestid + '></div></li>';
            }
            else
            {
                newdata = "<li id=" + deleteid + "><a href='#' id=" + latestid + " >" + $('#group' + value).find('option:selected').text() + "</a><img name=" + deleteid + "  class='cancel' ng-click='deletelink($event);' src='/icons/cancel.png'/><div id=" + 'info' + latestid + '></div></li>';
            }
             
            
            $('#ul' + value).append($compile(newdata)($scope));
            $('#group' + value).find('option:selected').remove()
            
        }

        $scope.deletelink = function ($event) {

            var value = $event.target.name;
            var SouceId = value.split("-")[0];
            var ParentId = value.split("-")[1];
            var ChildId = value.split("-")[2];
            var deleteid=SouceId+"-"+ParentId+"-"+ChildId;
            console.log("123", $rootScope.linkinglist)
            for (var s = 0; s < $rootScope.linkinglist.length; s++) {

                if ($rootScope.linkinglist[s].SouceId == SouceId && $rootScope.linkinglist[s].ParentId == ParentId && $rootScope.linkinglist[s].ChildId == ChildId) {

                   delete $rootScope.linkinglist[s];
                    
                }
            }
            console.log("12", $rootScope.linkinglist)
            item = {}
            item["records"] = $rootScope.linkinglist;
            $.ajax({
                url: Ecom_Api + 'Write?path=' + Link_list + '&json=' + JSON.stringify(item),
                type: 'POST',
                success: function (response) {

                    $("#" + deleteid).remove();
                },
                error: function () {
                }
            });
        }
        function Groupdata(group, ParentId, filterlist, attrid) {
            var html = "";
            if (group == "1") {
                var categories = $rootScope.catlist;
                for (var x = 0; x < categories.length; x++) {

                    if (categories[x].Attribute == attrid && categories[x].Flag != 0 && categories[x].Id != ParentId && $.inArray(categories[x].Id, filterlist) == -1)
                        html += "<option  value=" + categories[x].Id + ">" + categories[x].Name + "</option>";

                }
                
            }
            else {
                var items = $rootScope.itemlist;
                for (var x = 0; x < items.length; x++) {


                    if ( items[x].Id != ParentId && $.inArray(items[x].Id, filterlist) == -1)
                        html += "<option value=" + items[x].Id + ">" + items[x].Name + "</option>";

                }


            }
            
            return html;

        }
       

       

    }
    

   

    
   

    
});







